# Evil Morty

**The rutheless discord server killer**

## Adding It To A Server

It is very simple to add Donut Bot to your own server! To get started, simply have the **Manage Server** permission on your discord server. If you have this permission, or own the server. You can click the link below to add it to your server!

<https://discordapp.com/oauth2/authorize/?permissions=2146958591&scope=bot&client_id=357632722715279370>

### Commands

https://gitlab.com/Patchy319/EvilMorty/wiki/Commands


## Built With

* [Node.js](https://nodejs.org/) - Key to getting this up and running
* [Discord.js](https://discord.js.org/#/) - Key as well
* [NPM](https://www.npmjs.com/) - Lots of NPM packages were used in order to add extra functionality


## Author

* **[Patchy319](https://gitlab.com/Patchy319/)**
* [Full List](https://gitlab.com/Patchy319/EvilMorty/graphs/master)


### Acknowledgments

* None for this one!